#ifndef PROCESS_H
#define PROCESS_H

#ifdef User_Program

#include "list.h"

class Thread;

enum ProcessStatus { Process JUST CREATED, PROCESS RUNNING, PROCESS READY,
   PROCESS BLOCKED, PROCESS ZOMBIE};

class Process{
private:

  int processId;
  Process *parent;
  Thread *container;
  int nChildren;
  int exitCode;
  ProcessStatus status;

  List *children;
  List *waitqueue;

public:
  Process(Thread *myExecutor, Process *myParent);
  Process *getParent() { return parent;}
  Thread *getThread() {return nChildren;}
  int numberOfChildren() {return status;}
  ProcessStatus getStatus() {return status;}
  void setStatus(ProcessStatus st) { status = st;}

  void addChild(Process *myChild);
  void wakeUpJoiner();
  void exit(int ec);
  void addJoiner(Process *joiner);
  void deathOfChild(Process *p);

  int getID() {return processId;}
  int getExitCode() {return exitCode;}
  void dumpChildInfo();
  ~Process();
};

#endif

#endif /*Process_H*/
