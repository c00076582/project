// threadtest.cc
//	Simple test case for the threads assignment.
//
//	Create two threads, and have them context switch
//	back and forth between themselves by calling Thread::Yield,
//	to illustrate the inner workings of the thread system.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "synch.h"
#include <ctype.h>
#include "iostream"
#include "math.h"
#include <string.h>
#include <stdio.h>
using namespace std;

struct ACCESS {
	int id;
	bool READ = FALSE;
	bool WRITE = FALSE;
	bool SWITCH  = FALSE;
	bool EXECUTE = FALSE;
	bool PRINT = FALSE;
	int switchID;
};

ACCESS **accessMatrix;
ACCESS function;
void
READ(int which)
{
	printf("Domain %d: Attempts to from object: %d\n", which, function.id);
	if(function.READ){
		printf("Domain %d has read from object %d!\n", which, function.id);
	}
	else {
		printf("Domain %d was not allowed access to read from object %d\n", which, function.id);
	}
}

void
WRITE(int which)
{
	printf("Domain %d: Attempts to write to object: %d\n", which, function.id);
	if(function.WRITE){
		printf("Domain %d has written into object %d!\n", which, function.id);
	}
	else {
		printf("Domain %d was not allowed access to write in object %d\n", which, function.id);
	}
}

void
SWITCH(int which)
{
	printf("Domain %d: Attempts to switch to Domain: %d\n", which, function.switchID);
	if(function.SWITCH){
		printf("Domain %d successfully switched to Domain %d!\n", which, function.switchID);
	}
	else {
		printf("Domain %d was not allowed to switch to Domain %d\n", which, function.switchID);
	}
}

void
EXECUTE(int which)
{
	printf("Domain %d: Attempts to execute from object: %d\n", which, function.id);
	if(function.EXECUTE){
		printf("Domain %d has read into object %d!\n", which, function.id);
	}
	else {
		printf("Domain %d was not allowed access to read object %d\n", which, function.id);
	}
}

void
PRINT(int which)
{
	printf("Domain %d: Attempts to print from object: %d\n", which, function.id);
	if(function.PRINT){
		printf("Domain %d has printed from object %d!\n", which, function.id);
	}
	else {
		printf("Domain %d was not allowed to print from object %d\n", which, function.id);
	}
}


long
Validate()  {
	char  *input = new char[10000];
	for (int i =0; i <10000;i++){
		*(input + i) = NULL;
	}

	bool next = true;

	while(next){
		cin >> input;
		bool String = false;
		bool Integer = false;
		bool skip = false;
		if (*(input+1) == NULL){
			if (input[0] >= '0' && input[0] <='9'){
				Integer = true;
			}
			skip = true;
		}
		for (int i = 0; i<10000; i++){
			if (skip || (input+i) == NULL){
				break;
			}
			else if (String){
				break;
			}
			else if (*(input+i) >= '0' && *(input+i) <= '9'){
				Integer = true;
			}
			else if (*input == '0'){
				Integer = true;
				break;
			}
			else {
				String = true;
			}
		}

		if (Integer && !String){
			break;
		}
		cout << "Value must be 0 or more. Please try again: ";
	}
	return atoi(input);
}

void
Matrix(int which){
// 	printf("Please enter the amount of domains:  \n");
// 	int DOMAINS = Validate();
// 	printf("Please enter the amount of objects:  \n");
// 	int OBJECTS = Validate();
// 	if (DOMAINS == 0 && OBJECTS == 0){
// 		printf("Your domain value is: %d\n", DOMAINS);
// 		printf("Your objects value is: %d\n", OBJECTS);
// 		printf("Matrix was not made. Exiting!!!\n");
// 		currentThread->Finish();
// 	}
// 	int rows = DOMAINS + OBJECTS-1;
// 	ACCESS tempMatrix[DOMAINS][(DOMAINS+OBJECTS-1)];
//
// 	accessMatrix = new ACCESS[DOMAINS][(DOMAINS+OBJECTS-1)];
// 	int random = Random()%5;
// 	for (int i = 0; i < DOMAINS; i++){
// 		printf("Domain %d \n ", i);
// 		for (int j =0; j < rows; j++){
// 			printf("Object %d \n ", j);
//
// 			if (j < OBJECTS){
// 				if (random == 1) {
// 					accessMatrix[i][j].READ = TRUE;
// 				}
// 				random = Random()%5;
// 				if (random == 1){
// 					accessMatrix[i][j].WRITE = TRUE;
// 				}
// 				random = Random()%5;
//
// 				if (random == 1){
// 					accessMatrix[i][j].EXECUTE = TRUE;
// 				}
// 				random = Random()%5;
// 				if (random == 1){
// 					accessMatrix[i][j].PRINT = TRUE;
// 				}
// 			}
// 			else {
// 				if (random == 1){
// 					accessMatrix[i][j].SWITCH = TRUE;
//
// 				}
// 				accessMatrix[i][j].switchID = (j-OBJECTS-1);
// 			}
// 			random = Random()%5;
//
//
// 		}
// 	}
 }

void
ThreadTest(){
	Thread *f = new Thread("");
	f->Fork(Matrix, 1);
}

// void
// ThreadTest()
// {
// 	printf("print\n")
// }
//
